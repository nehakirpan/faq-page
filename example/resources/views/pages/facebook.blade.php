@extends('welcome')

@section('title')
    
@endsection()

@section('css')
    @vite(['resources/sass/facebook.scss'])
@endsection()

@section('content')
    <main>
        <section class="container">
            <div class="logo">
                <img src="/images/Facebook-Logo.png" alt="image">
            </div>
            <div class="login-card">
                <P>Log in to Facebook </P>
                <form class="login-form">
                    
                    <input type="text" name="email" placeholder="Email Address or Phone Number">
                    <input type="password" name="password" placeholder="Password">
                    <input type="button" name="button" value="Log in" class="button">
                </form>
                <div class="link">
                    <a href="#">Forgotten Password?</a>
                    <a href="#">Sign Up for Facebook</a>
                </div>
                <div class="hr">
                    <hr>
                    <p class="p">or</p>
                    <hr>
                </div>
                <button class="f-btn">Create New Account</button>
            </div>
            <div class="main-footer">
            <div class="footer">
                <div class="div-1">
                    <div class="lan">English (UK)</div>
                    <div class="lan">हिन्दी</div>
                    <div class="lan">ગુજરાતી</div>
                    <div class="lan">ਪੰਜਾਬੀ</div>
                </div>
                
                <div class="div-2">
                    <div class="lan">मराठी</div>
                    <div class="lan">اردو</div>
                    <div class="lan">ಕನ್ನಡ</div>
                    <div class="lan"><button>+</button></div>
                </div>   
            </div>
            <hr class="f-hr">
            <div class="footer2">
                <p>Sign Up Log in Messenger Facebook Lite Video Places Games Marketplace Meta PayMeta StoreMeta Quest Instagram Threads Fundraisers Services Voting Information CentrePrivacy PolicyPrivacy CentreGroups About Create adCreate Page Developers Careers Cookies AdChoices Terms HelpContact uploading and non-users Settings Activity log</p>
            </div>
            <p class="footer-text">Meta © 2023</p>
            </div>
        </section>
    </main>
@endsection()





