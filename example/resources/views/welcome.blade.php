<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

       @yield('title')

        @yield('css')

        @vite(['resources/sass/facebook.scss'])
        
    </head>
    <body class="antialiased">
        @yield('content')
        
    </body>
</html>
